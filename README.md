# BDD with SpecFlow: Restructuring

**Estimated reading time**: 15 min

## Story Outline
In this coding story, you will learn how to reconfigure the initial Page class to make it suitable for BDD approach.

## Story Organization
**Story Branch**: main
> `git checkout main`

Tags: #Selenium, #.NET, #C_Sharp, #BDD, #SpecFlow
